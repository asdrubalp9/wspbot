import sys
from selenium import webdriver
from time import sleep
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
#https://www.youtube.com/watch?v=W2kAF9pKPPE
from tkinter import ttk
from tkinter import messagebox
from tkinter import *
import os
import requests
import json
import urllib.request
import clipboard
import threading
if os.name == 'nt':
    import winsound

class wspBot():
    def __init__(self):
        chrome_options = webdriver.ChromeOptions()
        #prefs = { "profile.default_content_setting_values.notifications" : 2}
        chrome_options.add_experimental_option( "prefs" , { 
            "profile.default_content_setting_values.notifications" : 2,
            'credentials_enable_service': False,
            'profile': {
                'password_manager_enabled': False
            }
        })
        chrome_options.add_experimental_option("excludeSwitches", ['enable-automation'])
        if os.name == 'nt':
            location = './chromedriver.exe'
        else:
            location = '/usr/bin/chromedriver'
        self.driver = webdriver.Chrome(executable_path=location,chrome_options=chrome_options)
        main_url = "https://wa.me/56964182850"
        self.driver.get(main_url)
        wait = WebDriverWait(self.driver, 5)
        element = wait.until(EC.element_to_be_clickable((By.XPATH,'//*[@id="action-button"]')))
        print(element)
        element.click()


if __name__ == '__main__':
    chrome_options = webdriver.ChromeOptions()
    #prefs = { "profile.default_content_setting_values.notifications" : 2}
    chrome_options.add_experimental_option( "prefs" , { 
        "profile.default_content_setting_values.notifications" : 2,
        'credentials_enable_service': False,
        'profile': {
            'password_manager_enabled': False
        }
    })
    dir_path = os.getcwd()
    profile = os.path.join(dir_path, "profile", "wpp")
    chrome_options.add_argument(
            r"user-data-dir={}".format( profile ) )
    chrome_options.add_experimental_option("excludeSwitches", ['enable-automation'])
    if os.name == 'nt':
        location = './chromedriver.exe'
    else:
        location = '/usr/bin/chromedriver'
    driver = webdriver.Chrome(executable_path=location,chrome_options=chrome_options)
    #main_url = "https://wa.me/56964182850"
    main_url = "https://web.whatsapp.com/"
    
    driver.get(main_url)
    wait = WebDriverWait(driver, 15)
    main_url = "https://web.whatsapp.com/send?phone=56945209430&text&source&data"
    driver.get(main_url)
    
    textHolder = wait.until(EC.element_to_be_clickable((By.XPATH,'//*[@id="main"]/footer/div[1]/div[2]/div')))
    
    textHolder.click()
    sleep(0.5)
    textHolder.send_keys('Hola jefe, espere un rato y le mando un videito como plan B.')
    textHolder.send_keys(Keys.RETURN)
    sendButton = wait.until(EC.element_to_be_clickable((By.XPATH,'//*[@id="main"]/footer/div[1]/div[3]/button')))
    sendButton.click()
    sleep(999)